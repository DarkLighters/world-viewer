<?php if (isset($infos[0]["name"])){ ?>
	<div class="grid-item">
		<h2> <?php echo $infos[0]["name"]; ?></h2>
		<br />
		<table>
			<thead>
				<tr>
					<th>Capital</th>
					<th>Languages</th>
					<th>Currency</th>
					<th>Demonym</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><?php echo $infos[0]["capital"]; ?></td>
					<td><?php echo $infos[0]["languages"]; ?></td>
					<td><?php echo $infos[0]["currency"]; ?></td>
					<td><?php echo $infos[0]["demonym"]; ?></td>
				</tr>
			</tbody>
		</table>
	</div>
<?php } ?>