<center>
<link href="/Hackathon/assets/js/jqvmap/dist/jqvmap.css" media="screen" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="/Hackathon/assets/js/jqvmap/dist/jquery.vmap.js"></script>
<script type="text/javascript" src="/Hackathon/assets/js/jqvmap/dist/maps/jquery.vmap.world.js" charset="utf-8"></script>
<script type="text/javascript" src="/Hackathon/assets/js/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>

<script>
  jQuery(document).ready(function () {
	jQuery('#vmap').vectorMap({

	  map: 'world_en',
	  backgroundColor: '#eee',
	  color: '#ffffff',
	  hoverOpacity: 0.7,
	  selectedColor: '#666666',
	  enableZoom: true,
	  showTooltip: true,
	  scaleColors: ['#CAED89', '#039100'],
	  values: sample_data,
	  normalizeFunction: 'polynomial',

	  onRegionClick: function(element, code, region)
{
	window.location.replace("/Hackathon/index.php/country/environnement/country/"+region+"/code/"+code);
}
	});
  });
</script>
<div id="vmap" style="width: 1000px; height: 500px;"></div>
<p>Welcome on the Data World website ! <br /> A web application based on a large ammount of open data sets that allow you to learn a lot of things about every countries.</p>
</center>