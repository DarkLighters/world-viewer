<link href="/Hackathon/assets/js/jqvmap/dist/jqvmap.css" media="screen" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="/Hackathon/assets/js/jqvmap/dist/jquery.vmap.js"></script>
<script type="text/javascript" src="/Hackathon/assets/js/jqvmap/dist/maps/jquery.vmap.world.js" charset="utf-8"></script>
<script type="text/javascript" src="/Hackathon/assets/js/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>

<script>
  jQuery(document).ready(function () {
	jQuery('#vmap').vectorMap({

	  map: 'world_en',
	  backgroundColor: '#eee',
	  color: '#ffffff',
	  hoverOpacity: 0.7,
	  selectedColor: '#666666',
	  enableZoom: true,
	  showTooltip: true,
	  scaleColors: ['#CAED89', '#039100'],
	  values: sample_data,
	  normalizeFunction: 'polynomial',

	  onRegionClick: function(element, code, region)
{
	window.location.replace("/Hackathon/index.php/country/<?php echo $category; ?>/country/"+region+"/code/"+code);
}
	});
		$('#vmap').vectorMap('select', '<?php if(isset($code))echo $code;?>');
  });
</script>

<script type="text/javascript" src="/Hackathon/assets/masonry.pkgd.js"></script>
<script>
    $(document).ready(function () {
        $('.grid').masonry({
            itemSelector: '.grid-item',
            columnWidth: '.grid-sizer',
            percentPosition: true,
            gutter: 50
        });
    });
</script>
<!--h3 class="title_section"><img style="width:24px;" src="/Hackathon/assets/img/world.png" /> Environnement</h3-->
<div class="grid">
    <div class="grid-sizer"></div>
    <div class="grid-item">
		<div id="vmap" style="width: 800px; height: 400px;"></div>
	</div>