<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="/Hackathon/assets/css/style.css" />
		<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"> -->
		<meta charset="UTF-8" />
		<title>DataWorld</title>
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	</head>
	<body>
		<div id="menu">
			Data World<?php if(isset($category)){echo ' - '.ucfirst($category).' - '.ucfirst(str_replace('%20', ' ', $country));} ?>
		</div>
		<?php if (isset($country)){ ?>
			<div id="barre_v">
				<?php if(!isset($code)){ ?>
					<p class="active"><a href="/Hackathon/index.php/country/index"><img title="Home" style="width:30px;" src="/Hackathon/assets/img/home2.png" /></a></p>
					<p><a href="/Hackathon/index.php/country/environnement<?php if(isset($code))echo '/country/'.$country.'/code/'.$code;?>"><img title="Environment" style="width:24px;" src="/Hackathon/assets/img/world_b.png" /></a></p>
					<p><a href="/Hackathon/index.php/country/demography<?php if(isset($code))echo '/country/'.$country.'/code/'.$code;?>"><img title="Economy" style="width:24px;" src="/Hackathon/assets/img/group2_b.png" /></a></p>
					<p><a href="/Hackathon/index.php/country/economy<?php if(isset($code))echo '/country/'.$country.'/code/'.$code;?>"><img title="Demography" style="width:24px;" src="/Hackathon/assets/img/courbe_b.png" /></a></p>
				<?php } else {
					if($category == "environnement"){ ?>
						<p><a href="/Hackathon/index.php/country/index"><img title="Home" style="width:30px;" src="/Hackathon/assets/img/home2.png" /></a></p>
						<p class="active"><a href="/Hackathon/index.php/country/environnement<?php if(isset($code))echo '/country/'.$country.'/code/'.$code;?>"><img title="Environment" style="width:24px;" src="/Hackathon/assets/img/world_b.png" /></a></p>
						<p><a href="/Hackathon/index.php/country/demography<?php if(isset($code))echo '/country/'.$country.'/code/'.$code;?>"><img title="Economy" style="width:24px;" src="/Hackathon/assets/img/group2_b.png" /></a></p>
						<p><a href="/Hackathon/index.php/country/economy<?php if(isset($code))echo '/country/'.$country.'/code/'.$code;?>"><img title="Demography" style="width:24px;" src="/Hackathon/assets/img/courbe_b.png" /></a></p>
					<?php }else if ($category == "demography"){ ?>
						<p><a href="/Hackathon/index.php/country/index"><img title="Home" style="width:30px;" src="/Hackathon/assets/img/home2.png" /></a></p>
						<p><a href="/Hackathon/index.php/country/environnement<?php if(isset($code))echo '/country/'.$country.'/code/'.$code;?>"><img title="Environment" style="width:24px;" src="/Hackathon/assets/img/world_b.png" /></a></p>
						<p class="active"><a href="/Hackathon/index.php/country/demography<?php if(isset($code))echo '/country/'.$country.'/code/'.$code;?>"><img title="Economy" style="width:24px;" src="/Hackathon/assets/img/group2_b.png" /></a></p>
						<p><a href="/Hackathon/index.php/country/economy<?php if(isset($code))echo '/country/'.$country.'/code/'.$code;?>"><img title="Demography" style="width:24px;" src="/Hackathon/assets/img/courbe_b.png" /></a></p>
					<?php }else if($category == "economy"){ ?>
						<p><a href="/Hackathon/index.php/country/index"><img title="Home" style="width:30px;" src="/Hackathon/assets/img/home2.png" /></a></p>
						<p><a href="/Hackathon/index.php/country/environnement<?php if(isset($code))echo '/country/'.$country.'/code/'.$code;?>"><img title="Environment" style="width:24px;" src="/Hackathon/assets/img/world_b.png" /></a></p>
						<p><a href="/Hackathon/index.php/country/demography<?php if(isset($code))echo '/country/'.$country.'/code/'.$code;?>"><img title="Economy" style="width:24px;" src="/Hackathon/assets/img/group2_b.png" /></a></p>
						<p class="active"><a href="/Hackathon/index.php/country/economy<?php if(isset($code))echo '/country/'.$country.'/code/'.$code;?>"><img title="Demography" style="width:24px;" src="/Hackathon/assets/img/courbe_b.png" /></a></p>
					<?php }
				} ?>
			</div>
		<?php }else{ ?>
			<div id="barre_v">
				<p><a href="/Hackathon/index.php/country/index"><img title="Home" style="width:30px;" src="/Hackathon/assets/img/home2.png" /></a></p>
			</div>
		<?php } ?>
		

		<br /><br />