<?php
	// list of all parameters
	/*$xml_data = 
	$color = "#3983af";
	$value_legend = "CO2 Ton Reject";
	$width = 600;
	$height = 400;
	$group_width = 60%;
	$chart_title = "test";
	$key = "co2"*/
?>

  <script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart<?php echo $key; ?>);
    function drawChart<?php echo $key; ?>() {
      var data = google.visualization.arrayToDataTable([
        ["Year", "<?php echo $value_legend; ?>", { role: "style" } ],
        <?php
		 foreach($xml_data as $k=>$val) {
			if($k < count($xml_data)-1)
				echo '["'.$val['annee'].'", '.$val[$key].',"'.$color.'"],';
			else
				echo '["'.$val['annee'].'", '.$val[$key].',"'.$color.'"]]);';
		}
		?>

      var view = new google.visualization.DataView(data);

      var options = {
        title: "<?php echo $chart_title; ?>",
        width: <?php echo $width; ?>,
        height: <?php echo $height; ?>,
        bar: {groupWidth: "<?php echo $group_width; ?>%"},
        legend: { position: "right" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values<?php echo $key; ?>"));
      chart.draw(view, options);
  }
  </script>
<div class="grid-item" id="columnchart_values<?php echo $key; ?>" style="width: <?php echo $width; ?>px; height: <?php echo $height; ?>px;"></div>