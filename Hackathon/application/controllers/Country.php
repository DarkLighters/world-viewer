<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('EnvironementModel');
		$this->load->model('DemographyModel');
		$this->load->model('FinanceModel');
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');
	}

	public function environnement($data){
		$data=$this->uri->uri_to_assoc(3);
		$data['category'] = 'environnement';
		$this->load->view('header', $data);
		$this->load->view('carte', $data);
		$data['infos'] = $this->FinanceModel->getCountryInfos($data['country']);
		$this->load->view('info_country', $data);
		// CO2 chart
		$data['xml_data'] = array_reverse($this->EnvironementModel->getCountryCo2($data['country']));
		$data['color'] = "#3983af";
		$data['value_legend'] = "CO2 ton";
		$data['width'] = 450;
		$data['height'] = 300;
		$data['group_width'] = 60;
		$data['chart_title'] = "CO2 Rejects";
		$data['key'] = "co2";
		$this->load->view('charts/chart_number_fx_year', $data);

		$this->load->view('fin_div');
		$this->load->view('footer');
	}
	
	public function demography($data){
		$data=$this->uri->uri_to_assoc(3);
		$data['category'] = 'demography';
		$this->load->view('header', $data);
		$this->load->view('carte', $data);
		$data['infos'] = $this->FinanceModel->getCountryInfos($data['country']);
		$this->load->view('info_country', $data);
		
		// Population density
		$data['xml_data'] = array_reverse($this->DemographyModel->getCountryDensity($data['country']));
		$data['color'] = "#3983af";
		$data['value_legend'] = "Human/km2";
		$data['width'] = 450;
		$data['height'] = 300;
		$data['group_width'] = 60;
		$data['chart_title'] = "Population density";
		$data['key'] = "valeur";
		$this->load->view('charts/chart_number_fx_year', $data);
		
		$this->load->view('fin_div');
		$this->load->view('footer');
	}
	
	public function economy($data){
		$data=$this->uri->uri_to_assoc(3);
		$data['category'] = 'economy';
		$this->load->view('header', $data);
		$this->load->view('carte', $data);
		$data['infos'] = $this->FinanceModel->getCountryInfos($data['country']);
		$this->load->view('info_country', $data);
		
		// PIB
		$data['xml_data'] = array_reverse($this->FinanceModel->getCountryPib($data['country']));
		$data['color'] = "#3983af";
		$data['value_legend'] = "PIB";
		$data['width'] = 450;
		$data['height'] = 300;
		$data['group_width'] = 60;
		$data['chart_title'] = "PIB";
		$data['key'] = "valeur";
		$this->load->view('charts/chart_number_fx_year', $data);
		
		$this->load->view('fin_div');
		$this->load->view('footer');
	}
}
