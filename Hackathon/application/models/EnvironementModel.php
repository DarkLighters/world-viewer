<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class EnvironementModel extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');

	}

	public function openFile($name){
		$string = file_get_contents(base_url().'/assets/xml/'.$name.'.xml'); 

		$xml = new SimpleXMLElement($string);

		return $xml;
	}



	public function getCountryCo2($name){
		$name = str_replace('%20', ' ', $name);
		
		$xml = $this->openFile('Co2');
		$result = $xml->xpath('//field[.="'.$name.'"]/parent::*');

		$arr = json_decode( json_encode($result) , 1);

		$final = array();
		$tab = array();

		foreach ($arr as $key => $value) {
			foreach ($value as $key2 => $value2) {
					$final = array($key2 => $value2 );
					$temp2 = array('annee' => $final['field'][1], 'co2' => $final['field'][2]);
					array_push($tab, $temp2);				
			}
		}

		return $tab;	
	}

	public function getCountryForestation($name){
		$name = str_replace('%20', ' ', $name);

		$xml = $this->openFile('forestation');
		$result = $xml->xpath('//field[.="'.$name.'"]/parent::*');

		$arr = json_decode( json_encode($result) , 1);

		$final = array();
		$tab = array();

		foreach ($arr as $key => $value) {
			foreach ($value as $key2 => $value2) {
					$final = array($key2 => $value2 );
					$temp2 = array('annee' => $final['field'][1], 'valeur' => $final['field'][2]);
					array_push($tab, $temp2);				
			}
		}

		return $tab;
	}

	public function getCountrySanitary($name){
		$name = str_replace('%20', ' ', $name);

		$xml = $this->openFile('sanitary');
		$result = $xml->xpath('//field[.="'.$name.'"]/parent::*');

		$arr = json_decode( json_encode($result) , 1);

		$final = array();
		$tab = array();

		foreach ($arr as $key => $value) {
			foreach ($value as $key2 => $value2) {
					$final = array($key2 => $value2 );
					$temp2 = array('annee' => $final['field'][1], 'valeur' => $final['field'][2]);
					array_push($tab, $temp2);				
			}
		}

		return $tab;
	}

	public function getCountryWater($name){
		$name = str_replace('%20', ' ', $name);

		$xml = $this->openFile('water');
		$result = $xml->xpath('//field[.="'.$name.'"]/parent::*');

		$arr = json_decode( json_encode($result) , 1);

		$final = array();
		$tab = array();

		foreach ($arr as $key => $value) {
			foreach ($value as $key2 => $value2) {
					$final = array($key2 => $value2 );
					$temp2 = array('annee' => $final['field'][1], 'valeur' => $final['field'][2]);
					array_push($tab, $temp2);				
			}
		}

		return $tab;
	}


	

}