<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DemographyModel extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');

	}

	public function openFile($name){
		$string = file_get_contents(base_url().'/assets/xml/'.$name.'.xml'); 

		$xml = new SimpleXMLElement($string);

		return $xml;
	}

	public function getCountryDensity($name){
		$name = str_replace('%20', ' ', $name);
		
		$xml = $this->openFile('density');
		$result = $xml->xpath('//field[.="'.$name.'"]/parent::*');

		$arr = json_decode( json_encode($result) , 1);

		$final = array();
		$tab = array();

		foreach ($arr as $key => $value) {
			foreach ($value as $key2 => $value2) {
					$final = array($key2 => $value2 );
					$temp2 = array('annee' => $final['field'][1], 'valeur' => $final['field'][3]);
					array_push($tab, $temp2);				
			}
		}

		return $tab;
	}

	public function getCountryBirth($name){
		$name = str_replace('%20', ' ', $name);
		
		$xml = $this->openFile('liveBirthByAgeOfMotherAndSexChild');
		$result = $xml->xpath('//field[.="'.$name.'"]/parent::*');

		$arr = json_decode( json_encode($result) , 1);

		$final = array();
		$tab = array();
		

		foreach ($arr as $key => $value) {			
			foreach ($value as $key2 => $value2) {
					$final = array($key2 => $value2 );
					$temp2 = array('annee' => $final['field'][1], 'age' => $final['field'][4], 'value' => $final['field'][8]);
					array_push($tab, $temp2);		
			}
		}


		return $tab;	
	}

	public function getCountryUrban($name){
		$name = str_replace('%20', ' ', $name);
		
		$xml = $this->openFile('PopulationBySexAndUrbanRuralResidence');
		$result = $xml->xpath('//field[.="'.$name.'"]/parent::*');

		$arr = json_decode( json_encode($result) , 1);

		$final = array();
		$tab = array();

		foreach ($arr as $key => $value) {
			foreach ($value as $key2 => $value2) {
					$final = array($key2 => $value2 );
					$temp2 = array('annee' => $final['field'][1],'zone' => $final['field'][2], 'sex' => $final['field'][3], 'valeur' => $final['field'][7]);
					array_push($tab, $temp2);				
			}
		}
		//var_dump($tab);

		foreach ($tab as $k=>$val) {

			if($val['zone'] == 'Total' || $val['sex'] != 'Both Sexes'){
				echo "coucou\n";
				unset($tab[$k]);
			}
		}
		return $tab;

	}

	public function getCountryPopulation($name){
		$name = str_replace('%20', ' ', $name);
		$xml = $this->openFile('PopulationBySexAndUrbanRuralResidence');
		$result = $xml->xpath('//field[.="'.$name.'"]/parent::*');

		$arr = json_decode( json_encode($result) , 1);

		$final = array();
		$tab = array();

		foreach ($arr as $key => $value) {
			foreach ($value as $key2 => $value2) {
					$final = array($key2 => $value2 );
					$temp2 = array('annee' => $final['field'][1],'zone' => $final['field'][2], 'sex' => $final['field'][3], 'valeur' => $final['field'][7]);
					array_push($tab, $temp2);				
			}
		}
		$ok = true;
		foreach ($tab as $k=>$val) {

			if($val['zone'] != 'Total' || $val['sex'] != 'Both Sexes' && $ok !=false){
				echo "coucou\n";
				unset($tab[$k]);
				$ok = false;
			}
		}
		return $tab;
	}

		

}