<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FinanceModel extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');

	}

	public function openFile($name){
		$string = file_get_contents(base_url().'/assets/xml/'.$name.'.xml'); 

		$xml = new SimpleXMLElement($string);

		return $xml;
	}

	public function getCountryPib($name){
		$name = str_replace('%20', ' ', $name);
		
		$xml = $this->openFile('pib');
		$result = $xml->xpath('//field[.="'.$name.'"]/parent::*');

		$arr = json_decode( json_encode($result) , 1);

		$final = array();
		$tab = array();

		foreach ($arr as $key => $value) {
			foreach ($value as $key2 => $value2) {
					$final = array($key2 => $value2 );
					$temp2 = array('annee' => $final['field'][1], 'valeur' => $final['field'][2]);
					array_push($tab, $temp2);				
			}
		}

		return $tab;
	}

	public function getCountryInfos($name){
		$name = str_replace('%20', ' ', $name);
		$xml = $this->openFile('global_countries');
		$result = $xml->xpath('//country[@name="'.$name.'"]');

		$arr = json_decode( json_encode($result) , 1);

		$tab = array();
		$final = array();

		foreach ($arr as $key => $value) {
				foreach ($value as $key2 => $value2) {
					//var_dump($value2);
					$final = array('name' => $value2['name'], 'currency' => $value2['currency'], 'capital' => $value2['capital'], 'languages' => $value2['languages'], 'demonym' => $value2['demonym']);
					//$temp2 = array('annee' => $final['@attribute'][6]);
					array_push($tab, $final);
				}
		
		}

		return $tab;
	}


		

}